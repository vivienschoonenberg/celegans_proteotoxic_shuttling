###########
## Date created: Thursday 04 November 2021
##
## Author(s): Vivien A. C. Schoonenberg
##
## Email: v.schoonenberg@imb-mainz.de
##
############
library(plyr)
library(here)
conflict_prefer("here", "here")
library(dplyr)
library(KEGGREST)
library(clusterProfiler)
library(AnnotationHub)
library(ggplot2)
library(viridis)
#library(conflicted)

dir.create(here("results", "shiny-data"))

pal_cb <- c("#0173B2FF", "#029E73FF", "#D55E00FF", "#949494FF", "#ECE133FF", "#56B4E9FF", "#DE8F05FF", "#CC78BCFF", "#CA9161FF", "#FBAFE4FF")

# making a list of KEGG terms and pathways
CELE <- keggList("cel")
CELpathway  <-  keggLink("pathway", "cel")
CEL.df <- data.frame(keyName = names(CELpathway), path = CELpathway, row.names = NULL)
path_names  <- c()
for (path in unique(CEL.df$path)) {
    print(path)
    element  <- keggGet(path)[[1]]$PATHWAY_MAP
    path_names  <- c(path_names, element)
}
names.df <- data.frame(path_id = names(path_names), KEGG_term = path_names, row.names = NULL)

CEL.df$keyName  <- gsub("cel:", "", CEL.df$keyName)
CEL.df$path  <- gsub("path:", "", CEL.df$path)
kegg.df  <- left_join(CEL.df, names.df, by = c("path" = "path_id"))
kegg.df.1 <-  ddply(kegg.df, .(keyName),
      summarise,
      path = paste(unique(path), collapse = ";"),
      KEGG_term = paste(unique(KEGG_term), collapse = ";"))

# WB ids and genes:
wormbase_ids <- read.csv(here("data", "WormBaseIDnames.csv"))
# define KEGG identifiers
wormbase_ids$kegg_id <- sprintf("CELE_%s", wormbase_ids$new_txn_id)
df_kegg <- left_join(wormbase_ids, kegg.df.1, by = c("kegg_id" = "keyName"))
clusters  <-  read.csv(here("results", "shiny-data", "SOMs_cluster_values.csv"))
clusters.1  <- left_join(clusters, df_kegg[c("wbps_gene_id", "KEGG_term")], by = c("WormBase_ID" = "wbps_gene_id"))
clusters.1  <- relocate(clusters.1, c("KEGG_term"), .before = NES_Q40_L2)
write.csv(clusters.1, here("results", "shiny-data", "SOMs_cluster_values-01.csv"), row.names = FALSE)


# GO AND KEGG TERM ANALYSIS PER CLUSTER ####
# load ES SOMs
load(here("data", "SOM", "Protein_SOM_model_ES.RData")) # called som_model

# Set up for KEGG and GO analysis ####
# Do any of these sets specifically enrich for certain GO terms? With a pAdjustMethod = "BH" and pvalueCutoff = 0.05
# set up variables, starting with the universe
# Here you want to define what will be the "universe" against what you are comparing the over-representation in your results. We use any detected gene/protein in the experiment.
# Load data ####
pg.iBAQ <- read.csv(here("results", "ProteinGroups_ES_iBAQ.csv"))
universe <-  pg.iBAQ # Path to you protein/gene universe. In this example
universe.IDs <- unique(universe$WB_ID) # Keep the unique protein IDs
# define organism
org <- "org.Ce.eg.db"
# only if never ran before:
# BiocManager::install(org, character.only = TRUE) # takes a while to install
library(org, character.only = TRUE)

SOM_proteins  <- read.csv(here("results", "FLTS-analysis", "Protein_soms_filtered_ES.csv"))
SOM_proteins  <- filter(SOM_proteins, cluster != "NA")
names(SOM_proteins)[2] <- "group"
key <- read.csv(here("data", "SOM", "cluster_key.csv"), header = TRUE)
SOM_proteins.1 <- left_join(SOM_proteins, key, by = c("group" = "old_clust"))
SOM_proteins.2 <- subset(SOM_proteins.1, select = -c(group))

rel_set <- unique(SOM_proteins.2$cluster)
for (i in rel_set) {
    enriched <- SOM_proteins.2[which(SOM_proteins.2$cluster == i), ]
    enriched.IDs <- unique(enriched$X)
    enriched_GO <- enrichGO(gene = enriched.IDs,
                    OrgDb = org,
                    keyType = "ENSEMBL",
                    ont = "BP", # Ontology (can also be MF or CC or a combination)
                    universe = universe.IDs, # If you want to go for all id, not only detected then: "org.Ce.eg.db"
                    pAdjustMethod = "BH",
                    pvalueCutoff = 0.05)
    #reduce semantic redundancy
    enriched_GO_Filtered <- clusterProfiler::simplify(enriched_GO, cutoff = 0.7, by = "p.adjust", select_fun = min)
    pdf_name <- paste("GO_cluster_", as.character(i), ".png", sep = "")

    if (nrow(enriched_GO_Filtered) > 0) {
        png(file = here("results", "shiny-data", paste("barplot_", pdf_name, sep='')))
        print(barplot(enriched_GO_Filtered, showCategory = 18) + ggtitle(paste0("Cluster ", i)))
        dev.off()
    #   png(file = here("results", "SOM_GOs", paste("dotplot_", pdf_name, sep='')))
    #    print(dotplot(enriched_GO_Filtered, showCategory=30))
    #     dev.off()
    }

    df.kegg_genes <- left_join(enriched, wormbase_ids[c("wbps_gene_id", "kegg_id")], by= c("X" = "wbps_gene_id"))
    kegg_enriched <- unique(df.kegg_genes$kegg_id)
    enriched_KEGG <- enrichKEGG(gene = kegg_enriched,
                    organism = "cel",
                    pvalueCutoff = 0.05)
    #reduce semantic redundancy
    pdf_name <- paste("KEGG_cluster_", as.character(i), ".png", sep = "")

    if (nrow(enriched_KEGG) > 0) {
        p1 <- barplot(enriched_KEGG, showCategory = 18) + 
            scale_fill_viridis(direction = 1) + 
            ggtitle(paste0("Cluster ", i))

        png(file = here("results", "shiny-data", paste("barplot_", pdf_name, sep = "")))
        print(p1)
        dev.off()
    #     png(file = here("results", "SOM_KEGGs", paste("dotplot_", pdf_name, sep='')))
    #      print(dotplot(enriched_KEGG, showCategory=30))
    #      dev.off()
    }
}


# FOR VOLCANO PLOTS ####
pg.iBAQ <- read.csv(here("results", "ProteinGroups_CalculatedVals_ES_iBAQ.csv"))
pg_slice  <- dplyr::select(pg.iBAQ, WB_ID, Gene.names, contains("pVal") & matches("Q40|CTRL"), contains("ES") & matches("Q40|CTRL"), contains("FoldChange") & matches("Q40|CTRL"))
head(pg_slice)
pg_slice <- left_join(pg_slice, wormbase_ids[c("wbps_gene_id", "new_txn_id")], by = c("WB_ID" = "wbps_gene_id"))
pg_slice.1 <- dplyr::mutate(pg_slice, Gene.names = ifelse(startsWith(pg_slice$Gene.names, "WB"), pg_slice$new_txn_id, pg_slice$Gene.names))
pg_slice.2 <- select(pg_slice.1, -c("new_txn_id"))

write.csv(pg_slice.2, here("results", "shiny-data", "Values_comparisons.csv"), row.names = FALSE)


all_values  <- c("soc-2", "WBGene00007070", "aly-2")

ID_names  <- select(pg_slice.2, c("WB_ID", "Gene.names"))
ID_names.slice  <- filter_all(ID_names, any_vars(. %in% all_values))
